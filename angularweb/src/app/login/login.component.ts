import { LoginForm } from './forms/login.form';
import { FormGroup } from '@angular/forms';
import { AuthGuardService } from './../guards/auth-guard.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email: string;
  public senha: string;
  public form: FormGroup;

  constructor(
    private authGuardService: AuthGuardService,
    private formLogin: LoginForm,
    private location: Location
  ) {
    this.form = this.formLogin.createForm();
  }

  ngOnInit() {
  }


  public getDadosForm(): any {
    return Object.assign(
        this.form.getRawValue()
    );
  }

  public autenticaAplicacao(){
    const dados = this.getDadosForm();
    this.email = dados.email;
    this.senha = dados.senha;
    if (this.email === 'email@email.com' && this.senha === 'admin'){
      this.authGuardService.autenticaAplicacao();
      this.location.go('dragon');
    }
  }


}
