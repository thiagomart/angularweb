import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Injectable()
export class LoginForm {

    constructor(private formBuilder: FormBuilder) {
    }

    public createForm(): FormGroup {
        return this.formBuilder.group({
            email: this.formBuilder.control('', [Validators.required]),
            senha: this.formBuilder.control('', [Validators.required])
        });
    }
  }
