import { DragonModel } from './../models/dragon.model';
import { Component, OnInit } from '@angular/core';
import { DragonService } from './../dragon/service/dragon.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../shared/service/notification/service/notification.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-dragon-delete',
  templateUrl: './dragon-delete.component.html',
  styleUrls: ['./dragon-delete.component.css']
})
export class DragonDeleteComponent implements OnInit {

  public dragon: DragonModel
  constructor(
    private route: ActivatedRoute,
    private service: DragonService,
    private location: Location,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.deleteDragon();
  }


  public deleteDragon(): void{
    const id = this.route.snapshot.paramMap.get('id');
    this.service.deleteDragon(id)
    .subscribe(
      dragon => {
        this.dragon = dragon;
        this.location.back();
      },
      error => {
        this.notificationService.errorMessage(error);
      });
  }

}
