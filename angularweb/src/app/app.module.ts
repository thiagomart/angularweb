import { LoginForm } from './login/forms/login.form';
import { NotificationService } from './shared/service/notification/service/notification.service';
import { DragonService } from './dragon/service/dragon.service';
import { DragonRestService } from './dragon/service/dragon-rest.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DragonComponent } from './dragon/dragon.component';
import { DragonListComponent } from './dragon-list/dragon-list.component';
import { HttpClientModule } from '@angular/common/http';
import { DragonDetailComponent } from './dragon-detail/dragon-detail.component';
import { DragonCreateComponent } from './dragon-create/dragon-create.component';
import { NotificationComponent } from './shared/service/notification/notification.component';
import { DragonCreateForm } from './dragon-create/form/dragon-create.form';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DragonDeleteComponent } from './dragon-delete/dragon-delete.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    DragonComponent,
    DragonListComponent,
    DragonDetailComponent,
    DragonCreateComponent,
    NotificationComponent,
    HeaderComponent,
    FooterComponent,
    DragonDeleteComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DragonService,
    DragonRestService,
    NotificationService,
    DragonCreateForm,
    AuthGuardService,
    LoginForm
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
