import { AuthGuardService } from './../guards/auth-guard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public autenticado: boolean;

  constructor(
    private authGuard: AuthGuardService
  ) { }

  ngOnInit() {
  }

  public getAutenticado(){
    return this.authGuard.canActivate();
  }
}
