import { DragonService } from './../dragon/service/dragon.service';
import { DragonModel } from './../models/dragon.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dragon-list',
  templateUrl: './dragon-list.component.html',
  styleUrls: ['./dragon-list.component.css']
})
export class DragonListComponent implements OnInit {

  public dragons: DragonModel[];

  constructor(
    private dragonService: DragonService
  ) { }

  ngOnInit() {
    this.carregarConteudo();
  }

  public carregarConteudo(): void{
    this.dragonService.listarTodosDragoes().subscribe(
      res => {
        this.dragons = res;
      });
  }


}
