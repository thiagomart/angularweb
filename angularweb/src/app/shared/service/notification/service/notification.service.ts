import { NotificationComponent } from './../notification.component';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notification: NotificationComponent;

  public add(notification: NotificationComponent): void {
      this.notification = notification;
  }

  public errorMessage(message: string): NotificationComponent {
      if (this.notification) {
          this.notification.errorMessage(message);
      }
      return this.notification;
  }
}
