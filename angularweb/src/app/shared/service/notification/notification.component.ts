import { Component, OnInit } from '@angular/core';
import { NotificationService } from './service/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  public titulo: string;
  public mensagens: string[] = [];
  public ativo: boolean = false;

  constructor(private notification: NotificationService) { }

  ngOnInit() {
    this.notification.add(this);
  }

  public errorMessage(message: string): void {
    this.titulo = 'Erro';
    this.mensagens.push(message);
    this.abrirModal();
  }

  public abrirModal(): void {
    this.ativo = true;
  }

  public confirmar(){
    this.mensagens = [];
    this.ativo = false;
  }

}
