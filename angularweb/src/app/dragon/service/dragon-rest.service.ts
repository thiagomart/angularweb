import { DragonModel } from './../../models/dragon.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class DragonRestService {

  constructor(private http: HttpClient) { }

  public getAllDragons(): Observable<DragonModel[]> {
    return of({})
    .pipe(res => this.http.get<DragonModel[]>(`${environment.END_POINT}`));
  }

  public getDragonById(id: string): Observable<DragonModel>{
    return of({})
    .pipe(res => this.http.get<DragonModel>(`${environment.END_POINT}/${id}`));
  }

  public createDragon(model: DragonModel): Observable<DragonModel>{
    return of({})
    .pipe(res => this.http.post<DragonModel>(`${environment.END_POINT}`, model));
  }

  public editDragon(model: DragonModel): Observable<DragonModel>{
    return of({})
    .pipe(res => this.http.put<DragonModel>(`${environment.END_POINT}/${model.id}`, model));
  }

  public deleteDragon(id: string): Observable<DragonModel>{
    return of({})
    .pipe(res => this.http.delete<DragonModel>(`${environment.END_POINT}/${id}`));
  }

}
