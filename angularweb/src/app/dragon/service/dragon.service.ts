import { DragonModel } from './../../models/dragon.model';
import { DragonRestService } from './dragon-rest.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class DragonService {

  constructor(
    private dragonRestService: DragonRestService
  )
  {  }

  public listarTodosDragoes(): Observable<DragonModel[]> {
    return this.dragonRestService.getAllDragons();
  }

  public buscarDragaoPorId(id: string): Observable<DragonModel> {
    return this.dragonRestService.getDragonById(id);
  }

  public criarDragao(model: DragonModel): Observable<DragonModel> {
    return this.dragonRestService.createDragon(model);
  }

  public editarDragao(model: DragonModel): Observable<DragonModel> {
    return this.dragonRestService.editDragon(model);
  }

  public deleteDragon(id: string): Observable<DragonModel> {
    return this.dragonRestService.deleteDragon(id);
  }
}
