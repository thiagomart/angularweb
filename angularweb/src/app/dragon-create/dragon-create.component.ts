import { DragonCreateForm } from './form/dragon-create.form';
import { FormGroup } from '@angular/forms';
import { DragonModel } from './../models/dragon.model';
import { DragonService } from './../dragon/service/dragon.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NotificationService } from '../shared/service/notification/service/notification.service';

@Component({
  selector: 'app-dragon-create',
  templateUrl: './dragon-create.component.html',
  styleUrls: ['./dragon-create.component.css']
})
export class DragonCreateComponent implements OnInit {

  public dragon: DragonModel;
  public formDragon: FormGroup;
  public dragonCreate:boolean;

  constructor(
    private route: ActivatedRoute,
    private service: DragonService,
    private location: Location,
    private notificationService: NotificationService,
    private dragonfrom: DragonCreateForm
  ) {
    this.formDragon = this.dragonfrom.createForm();
    if(!!this.route.snapshot.data[0] && this.route.snapshot.data[0] != [] &&  this.route.snapshot.data[0] != undefined){
      console.log(this.route.snapshot.data[0]['create'])
      this.dragonCreate = this.route.snapshot.data[0]['create'];
    }
  }

  ngOnInit() {
  }

  public voltar(): void{
    this.dragonCreate = false;
    this.location.back();
  }

  public getDadosForm(): DragonModel {
    return Object.assign(
        this.formDragon.getRawValue()
    );
  }

  public salvar():void{
    this.service.criarDragao(this.getDadosForm())
    .subscribe(
      dragon => {
        this.voltar()
        this.dragonCreate = false;
      },
      error => {
        this.notificationService.errorMessage(error);
      });
  }


}
