import { DragonService } from './../dragon/service/dragon.service';
import { DragonModel } from './../models/dragon.model';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NotificationService } from '../shared/service/notification/service/notification.service';

@Component({
  selector: 'app-dragon-detail',
  templateUrl: './dragon-detail.component.html',
  styleUrls: ['./dragon-detail.component.css']
})
export class DragonDetailComponent implements OnInit {



  public dragon: DragonModel;
  constructor(
    private route: ActivatedRoute,
    private service: DragonService,
    private location: Location,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.getDragon();
  }

  public getDragon(): void{
    const id = this.route.snapshot.paramMap.get('id');
    this.service.buscarDragaoPorId(id)
    .subscribe(
      dragon => this.dragon = dragon,
      error => {
        this.notificationService.errorMessage(error);
      });
  }

  public voltar(): void{
    this.location.back();
  }
}
