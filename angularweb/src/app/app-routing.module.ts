import { LoginComponent } from './login/login.component';
import { DragonDeleteComponent } from './dragon-delete/dragon-delete.component';
import { DragonCreateComponent } from './dragon-create/dragon-create.component';
import { DragonDetailComponent } from './dragon-detail/dragon-detail.component';
import { DragonComponent } from './dragon/dragon.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes = [
  { path: 'dragon', component: DragonComponent, canActivate: [AuthGuardService] },
  { path: 'visualizar/:id', component: DragonDetailComponent, canActivate: [AuthGuardService] },
  { path: 'excluir/:id', component: DragonDeleteComponent, canActivate: [AuthGuardService]},
  { path: 'criar', component: DragonCreateComponent, data:[{create: true}], canActivate: [AuthGuardService] },
  //{ path: 'dragon', redirectTo: '/dragon', component: DragonComponent },
  { path: '', redirectTo: '', pathMatch: 'full', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
