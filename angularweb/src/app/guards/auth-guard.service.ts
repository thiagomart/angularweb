import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  path: import("@angular/router").ActivatedRouteSnapshot[];
  route: import("@angular/router").ActivatedRouteSnapshot;

  constructor() { }

  private autenticado: boolean = false;

  canActivate(){
    return this.autenticado;
  }
  public autenticaAplicacao(){
    this.autenticado = true;
  }
  public Logout(){
    this.autenticado = false;
  }
}
